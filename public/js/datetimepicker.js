$(document).ready(function () {
    $("#start_datepicker").datepicker();

    $("#start_datepicker").change(function () {
        $("#categid").attr('href', function () {
            $date = $('#start_datepicker').datepicker({dateFormat: 'yy-mm-dd'}).val();
            return this.href + '/' + formatDate($date) + '/' + null ;
        });
    });
});


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}