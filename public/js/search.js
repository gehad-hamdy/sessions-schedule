$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $('select[name="dropdown"]').change(function () {
        $searchVal = $(this).val();
        $("#categid").attr('href', function () {
            return this.href + '/' + null + '/' + $searchVal;
        });
    })
});
