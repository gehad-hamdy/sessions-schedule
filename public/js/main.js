$(function () {
    updatePrices();

    $(".form-control").on('change', function () {
        updatePrices();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".quantity").on('change', function () {
        var input = $(this);
        var componentName = input.parent( "td").prev('td').find(".form-control :selected").text();
        var quantityValue = input.val();
        $.ajax({
            url: "validate-quantity",
            type:'GET',
            data: {quantity : quantityValue, name : componentName,_token: $('#_token').val()},
            success: function(data) {
                if($.isEmptyObject(data.error)){
                    updatePrices();
                }
            },
            error:function (data) {
                input.val(1);
             alert(data.responseText);
            }
        });

    });

    function updatePrices() {
        var totalPrice = 0;
        $('tr.config-details').each(function () {
            quantity = $(this).find('.quantity').val();
            price = $(this).find(".form-control :selected").val();
            var componentPrice = quantity * price;
            $(this).find('td').eq(3).html(componentPrice / 100);
            totalPrice += componentPrice;
        });
        document.getElementById('total-price').innerHTML = totalPrice / 100;
    }

});
