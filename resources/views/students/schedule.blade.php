<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
</head>
<div style="max-width: 500px; margin-top: 100px; margin-left: 150px;">
    {!! Form::open(['url' => 'schedule', 'method' => 'POST','class'=>'contact100-form validate-form']) !!}
    <div class="form-group row">
        <label for="example-date-input" class="col-2 col-form-label">Start Date</label>
        <div class="col-10">
            <input class="form-control" type="date" name="start_date" value="" id="example-date-input">
        </div>
    </div>

    <div class="form-group row">
        <label for="example-number-input" class="col-2 col-form-label">Session Numbers</label>
        <div class="col-10">
            <input class="form-control" name="session_no" type="number" value="" id="example-number-input">
        </div>
    </div>

    <div class="form-group">
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Saturday]" value="1">
            Saturday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Sunday]" value="2">
            Sunday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Monday]" value="3">
            Monday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Tuesday]" value="4">
            Tuesday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Wednesday]" value="5">
            Wednesday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Thursday]" value="6">
            Thursday
        </label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="days[Friday]" value="7">
            Friday
        </label>
    </div>
    <button type="submit" class="btn btn-primary">Calculate</button>
    {!! Form::close() !!}
</div>
