<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fonts/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>


<div class="limiter">
    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100 ver2 m-b-110">
                <div class="table100-head">
                    <table>
                        <thead>
                        <tr class="row100 head">
                            <th class="cell100 column1">Date</th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div class="table100-body js-pscroll">
                    <table>
                        <tbody>
                        @if(isset($dates))
                            @for($i=0;$i<sizeof($dates);$i++)
                                <tr class="row100 body">
                                    <td class="cell100 column1">{!! $dates[$i] !!}</td>
                                </tr>
                            @endfor
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</html>