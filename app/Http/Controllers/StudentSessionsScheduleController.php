<?php

namespace App\Http\Controllers;

use App\Http\CalculateDateSessions;
use Illuminate\Http\Request;

class StudentSessionsScheduleController extends Controller
{

    public function show()
    {
        return view('students.schedule');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function savePosts(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'days' => 'required',
            'session_no' => 'required'
        ]);

        $submitted_data = $request->all();
        $calculateSession = new CalculateDateSessions();
        $dates = $calculateSession->calculate($submitted_data);

        return view('students.show', compact('dates'));
    }
}
