<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 7/16/2018
 * Time: 2:00 AM
 */

namespace App\Http;


class SortArray
{
    public function Sort($dateDay)
    {
        usort($dateDay, function ($time1, $time2) {
            if (strtotime($time1) < strtotime($time2))
                return 1;
            else if (strtotime($time1) > strtotime($time2))
                return -1;
            else
                return 0;
        });

        return $dateDay;
    }
}