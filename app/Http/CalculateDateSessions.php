<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 7/16/2018
 * Time: 3:29 AM
 */

namespace App\Http;


class CalculateDateSessions
{
    /**
     * @param $submitted_data
     * @return array|mixed
     */
    public function calculate($submitted_data)
    {
        $start_day = date('D', strtotime($submitted_data['start_date']));
        $daysNumber = sizeof($submitted_data['days']);
        $num_of_se_day = round(($submitted_data['session_no'] * 30) / $daysNumber);
        $dates = [];
        $days = [];
        $finalDates = [];
        foreach ($submitted_data['days'] as $key => $value) {
            $dayStart = date('Y-m-d', strtotime($key, strtotime($submitted_data['start_date'])));
            $dates[] = $dayStart;
            $finalDates[] = $dayStart;
            $days[] = $key;
        }

        $c = $daysNumber;
        for ($i = 0; $i < $daysNumber; $i++) {
            for ($j = 0; $j < $num_of_se_day; $j++) {
                $finalDates[] = date('Y-m-d', strtotime($days[$i] . ' next week' . $dates[$i]));
                $dates[$i] = $finalDates[$c];
                $c++;
            }
            $c = sizeof($finalDates);
        }

        $sortDate = new SortArray();
        $dates = $sortDate->Sort($finalDates);

        return $dates;
    }
}